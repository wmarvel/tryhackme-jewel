#!/usr/bin/env python3
import string

if __name__ == '__main__':
    """Generate potential /content jpg filenames (sans extension)
    
    Looking at the burpsuite site map after poking at the site a bit, the .jpg
    files the site loads from the /content directory all have three character
    names, with all characters from the uppercase alphabet. 
    
    I hypothesized that the upload will randomly generate a three character
    filename and then append the .jpg extension, and that I needed a list
    I could pass to gobuster to get the filename after I successfully uploaded
    a reverse shell payload.
    
    After actually getting a shell and pulling the source code down with 
    'base64 index.js > content/index.jpg' and downloading that, I confirmed
    that the filename generation chose a random three-character name from
    the uppercase alphabet.
    
    After I'd completed the room I realized that there was a downloadable file
    provided that is exactly the output this script generates.
    """
    for one in string.ascii_uppercase:
        for two in string.ascii_uppercase:
            for three in string.ascii_uppercase:
                print(f'{one}{two}{three}')
